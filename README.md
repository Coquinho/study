Here I have [uri online judge](https://urionlinejudge.com.br) problems, `sort` algorithms and some random studies not yet organized:

```
.
├── random-studies
├── README.md
├── sort-algorithms
└── uri
```
